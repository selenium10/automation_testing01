package com.testng.bt;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Test_Class {
	
	WebDriver driver = null;
	JavascriptExecutor javascript = null;
	WebDriverWait wait = null;
	WebElement element = null;

	@BeforeTest
	public void driverInitialization() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Haran\\eclipse-workspace\\Automation_03\\driver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("excludeSwitches", new String[] { "enable-automation" });
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		
	}

	@Test(enabled = true)
	public void javascript_domain() {
		driver.get("https://www.amazon.in/");
		javascript = (JavascriptExecutor) driver;
		String CurrentURLUsingJS = (String) javascript.executeScript("return document.domain");
		System.out.println("Current URL is :" + CurrentURLUsingJS);

	}

	@AfterTest
	public void tearDown() {
		driver.close();
	}

	@Test(enabled = true)
	public void manualAlert() {

		wait = new WebDriverWait(driver, 30);
		javascript = (JavascriptExecutor) driver;
		javascript.executeScript("alert('Manual alert test case execution is started');");
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}

	@Test(enabled = true)
	public void enableDisableElement() throws InterruptedException {
		driver.get("http://only-testing-blog.blogspot.com/2013/11/new-test.html");
		/*
		 * element = driver.findElementByXPath(
		 * "(//div[@id='post-body-2641311481947341581']//form)[1]//input[1]");
		 * wait.until(ExpectedConditions.visibilityOf(element)); element.click();
		 * element.sendKeys("Test word");
		 */
		javascript = (JavascriptExecutor) driver;
		String disablefname = "document.getElementsByName('fname')[0].setAttribute('disabled', '');";
		javascript.executeScript(disablefname);
		Thread.sleep(3000);
	}

	@Test(enabled = true)
	public void actionClass() throws InterruptedException {
		driver.get("https://demoqa.com/text-box");
		element = driver.findElement(By.id("userName"));
		element.click();
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.SHIFT);
		actions.sendKeys("k");
		actions.sendKeys("i");
		actions.sendKeys("r");
		actions.sendKeys("u");
		actions.sendKeys("b");
		actions.sendKeys("a");
		actions.sendKeys("h");
		actions.sendKeys("a");
		actions.sendKeys("r");
		actions.sendKeys("a");
		actions.sendKeys("n");
		actions.sendKeys(Keys.SPACE);
		actions.sendKeys("s");
		actions.sendKeys("u");
		actions.sendKeys("b");
		actions.sendKeys("r");
		actions.sendKeys("a");
		actions.sendKeys("m");
		actions.sendKeys("a");
		actions.sendKeys("n");
		actions.sendKeys("i");
		actions.sendKeys("a");
		actions.sendKeys("n");
		//actions.keyDown(Keys.valueOf("a"));
		actions.keyUp(Keys.SHIFT);
		actions.build().perform();
		Thread.sleep(3000);
		actions.sendKeys(Keys.TAB);
		actions.build().perform();
		javascript = (JavascriptExecutor) driver;
		String disablefname = "document.getElementById('userEmail').value='Kirubaharan93@gmail.com'";
		javascript.executeScript(disablefname);

		Thread.sleep(3000);

	}

	@Test(enabled = true)
	public void fontProperties() throws InterruptedException {
		driver.get("http://only-testing-blog.blogspot.com/2014/05/login.html");
		element = driver.findElement(By.xpath("//div[@id='Blog1']/div[1]/div//h2//span"));
		System.out.println(element.getText() + "\n" + "Above text font properties are :");
		String fontSize = element.getCssValue("font-size");
		System.out.println("Font size is :" + fontSize);
		String fontColor = element.getCssValue("color");
		System.out.println("Font color is :" + fontColor);
		String fontFamily = element.getCssValue("font-family");
		System.out.println("Font family is :" + fontFamily);
		String fontTextAlign = element.getCssValue("text-align");
		System.out.println("Font alignment is :" + fontTextAlign);

	}

	@Test(enabled = false)
	public void highlightMyElements() throws InterruptedException {
		driver.get("http://only-testing-blog.blogspot.com/2013/11/new-test.html");
		javascript = (JavascriptExecutor) driver;
		// WebElement highlightMyElement = (WebElement)
		// javascript.executeScript("arguments[0].setAttribute('style', 'background:
		// yellow; border: 4px solid red;");
		WebElement firstName = driver
				.findElement(By.xpath("//div[@id='post-body-2641311481947341581']/div[1]/form[1]/input[1]"));
		javascript.executeScript("arguments[0].setAttribute('style,'border: solid 4px red');", firstName);
		// highlightMyElement.findElement(By.xpath("//div[@id='post-body-2641311481947341581']/div[1]/form[1]/input[1]"));
		firstName.sendKeys("Kirubaharan");
		WebElement confirmationButton = driver.findElement(By.xpath("//div[@id='demo']//..//button[1]"));
		// highlightMyElement.findElement(By.xpath("//div[@id='demo']//..//button[1]"));
		javascript.executeScript("arguments[0].setAttribute('style,'border: solid 4px red');", confirmationButton);
		confirmationButton.click();
		Thread.sleep(3000);

	}

	@Test(enabled = true)
	public void assertEqualsMethodOne() {
		driver.get("http://only-testing-blog.blogspot.com/2014/01/textbox.html");
		String actualText = driver.findElement(By.xpath("//div[@id='Blog1']/div[1]/div/h2/span")).getText();
		System.out.println(actualText);
		Assert.assertEquals(actualText, "Tuesday, 28 January 2014");
		System.out.println("Assertequals method one passed");
	}

	@Test(enabled = true)
	public void assertEqualsWithTry() {
		System.out.println();
		driver.get("http://only-testing-blog.blogspot.com/2014/01/textbox.html");
		String actualText = null;
		try {
			Assert.assertEquals(actualText, "Tuesday, 28 January 2014");
		} catch (AssertionError e) {
			System.out.println(e);
		}
		System.out.println("Assertequals method two passed");
		actualText = driver.findElement(By.xpath("//div[@id='Blog1']/div[1]/div/h2/span")).getText();
		Assert.assertEquals(actualText, "Tuesday, 28 January 2014");
		System.out.println("Asserequals method two with try block");
	}

	@Test(enabled = false)
	public void assertEqualsWithoutTry() {
		System.out.println();
		driver.get("http://only-testing-blog.blogspot.com/2014/01/textbox.html");
		String actualText = null;
		Assert.assertEquals(actualText, "Tuesday, 28 January 2014");
		System.out.println("Assertequals method three passed");
		actualText = driver.findElement(By.xpath("//div[@id='Blog1']/div[1]/div/h2/span")).getText();
		Assert.assertEquals(actualText, "Tuesday, 28 January 2014");
		System.out.println("Asserequals method three with try block");

	}

	@Test(enabled = true)
	public void assertNotEquals() {
		System.out.println();
		driver.get("http://only-testing-blog.blogspot.com/2014/01/textbox.html");
		String actualText = null;
		Assert.assertNotEquals(actualText, "Tuesday, 28 January 2014");
		System.out.println("Assertnotequals method  passed");
		System.out.println();
		driver.get("http://only-testing-blog.blogspot.com/2014/01/textbox.html");
		actualText = driver.findElement(By.xpath("//div[@id='Blog1']/div[1]/div/h2/span")).getText();
		try {
		Assert.assertNotEquals(actualText, "Tuesday, 28 January 2014");
		}catch (AssertionError e) {
			System.out.println(e);
		}
		System.out.println("Assertnotequals method with equal value  passed");

	}
	
	@Test
	public void hardAssertMetods() {
		driver.get("http://only-testing-blog.blogspot.com/2014/02/attributes.html");
		WebElement chck1 = driver.findElement(By.id("check1"));
		WebElement chck2 = driver.findElement(By.id("check2"));
		boolean boolval1 = chck1.isSelected();
		boolean boolval2 = chck2.isSelected();
		System.out.println("Checkbox 1 is selected :"+boolval1+"\n"+"Checkbox 2 is selected :"+boolval2);
		Assert.assertTrue(boolval1, "Checkbox is not selected");
		try {
		Assert.assertTrue(boolval2, "Checkbox 2 is not selected");
		}catch (AssertionError e) {
			System.out.println(e);
		}
		System.out.println("Assert true is passed");
		
		
		//Null and not null
		WebElement txt1 = driver.findElement(By.id("text1"));
		WebElement txt2 = driver.findElement(By.id("text2"));
		System.out.println(txt1.getAttribute("disabled"));
		System.out.println(txt2.getAttribute("disabled"));
		Assert.assertNull(txt1.getAttribute("disabled"));
		try {
		Assert.assertNotNull(txt2.getAttribute("disabled"));
		}catch (AssertionError e) {
			System.out.println(e);
		}
		System.out.println("Assert null case passed");
		
		
	}

}
