package com.testng.bt;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Selenium_TestOne {
	WebDriver driver = null;
	FileReader Readerobj = null;
	Properties Propobj = null;
	WebDriverWait wait = null;

	@BeforeTest
	public void browserInvoke() throws IOException {
		Readerobj = new FileReader(System.getProperty("user.dir") + "\\Resources\\TableTest.properties");
		Propobj = new Properties();
		Propobj.load(Readerobj);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + Propobj.getProperty("GCdirectory"));
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("excludeSwitches", new String[] { "enable-automation" });
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Test
	public void firstScenario() throws InterruptedException {
		System.out.println();
		System.out.println("First scenario");
		System.out.println();
		driver.get(Propobj.getProperty("BaseURL"));
		driver.findElement(By.xpath(Propobj.getProperty("TableTabXpath"))).click();
		driver.findElement(By.xpath(Propobj.getProperty("PaginationXpath"))).click();
		wait = new WebDriverWait(driver, 20);
		

		List<WebElement> pageList = driver.findElements(By.xpath(Propobj.getProperty("PagesXpath")));
		for (WebElement webElement : pageList) {
			String pagename = webElement.getText();
			if (pagename.equalsIgnoreCase("")) {
				System.out.println("Invalid page link");
			} else {
				WebElement tableElement = driver.findElement(By.id(Propobj.getProperty("TableID")));
				wait.until(ExpectedConditions.visibilityOfAllElements(tableElement));
				List<WebElement> tableHeadList = driver.findElements(By.xpath(Propobj.getProperty("TableHead")));
				for (WebElement webElement2 : tableHeadList) {
					System.out.println(webElement2.getText());
				}
				List<WebElement> tableDataList = driver.findElements(By.xpath(Propobj.getProperty("TableDataXpath")));
				for (WebElement webElement3 : tableDataList) {
					System.out.println(webElement3.getText());

				}
				try {
					driver.findElement(By.xpath(Propobj.getProperty("NextPageXpath"))).click();

				} catch (ElementNotInteractableException e) { // TODO: handle exception
					System.out.println(e);

				}

			}

		}

	}

	@AfterTest
	public void Teardown() throws InterruptedException {
		Thread.sleep(5000);
		driver.close();
	}

	@Test(priority = 1)
	public void secondScenario() throws InterruptedException {
		System.out.println();
		System.out.println("Second scenario");
		driver.get(Propobj.getProperty("BaseURL"));
		driver.findElement(By.xpath(Propobj.getProperty("TableTabXpath"))).click();
		driver.findElement(By.xpath(Propobj.getProperty("TableSearchXpath"))).click();
		System.out.println(driver.findElement(By.xpath(Propobj.getProperty("TableSearchHead1"))).getText());
		List<WebElement> dataFilterList = driver.findElements(By.xpath(Propobj.getProperty("TaskFilterTableData")));
		System.out.println("Filter by data table size is :" + dataFilterList.size());
		List<WebElement> taskList = driver.findElements(By.xpath(Propobj.getProperty("TaskXpath")));
		String Expected[] = { "Wireframes", "Landing Page", "SEO tags", "Bootstrap 3", "jQuery library",
				"Browser Issues", "Bug fixing" };
		for (int i = 1; i <= taskList.size(); i++) {
			// driver.findElement(By.xpath("(//table[@id='task-table']//tbody//tr//td[2])["
			// + i + "]")).getText();
			String actual = driver.findElement(By.xpath(Propobj.getProperty("TaskTableFilterXpathpart1") + i
					+ Propobj.getProperty("TaskTableFilterXpathpart2"))).getText();
			System.out.println(actual);
			driver.findElement(By.id(Propobj.getProperty("TaskTableFilterID"))).sendKeys(actual);
			try {
				Assert.assertEquals(actual, Expected[i - 1], "Result mismatch");
			} catch (AssertionError e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			System.out.println(driver.findElement(By.xpath(Propobj.getProperty("TaskFilterResultPart1")+i+Propobj.getProperty("TaskFilterResultPart2"))).getText());
			driver.findElement(By.id(Propobj.getProperty("TaskTableFilterID"))).clear();
			driver.navigate().refresh();

			System.out.println("Test case passed without errors");

		}

	}

	@Test(priority = 2)
	public void ThirdScenario() {
		System.out.println();
		System.out.println("Third scenario");
		driver.get(Propobj.getProperty("BaseURL"));
		driver.findElement(By.xpath(Propobj.getProperty("TableTabXpath"))).click();
		driver.findElement(By.xpath(Propobj.getProperty("TableSearchXpath"))).click();
		System.out.println(driver.findElement(By.xpath(Propobj.getProperty("TableSearchHead2"))).getText());
		driver.findElement(By.xpath(Propobj.getProperty("FilterToggleButton2"))).click();
		List<WebElement> columnFilterList = driver.findElements(By.xpath(Propobj.getProperty("ColumnFilterXpath")));
		String Expected[] = { "Samuels", "Karano", "Swarroon", "Kathaniko", "Dimarison" };
		for (int i = 1; i <= columnFilterList.size(); i++) {
			String actual = driver.findElement(By.xpath(
					Propobj.getProperty("ColumnFilterXpathPart1") + i + Propobj.getProperty("ColumnFilterXpathPart2")))
					.getText();
			System.out.println(actual);
			driver.findElement(By.xpath(Propobj.getProperty("LastNameFilterXpath"))).sendKeys(actual);

			try {
				Assert.assertEquals(actual, Expected[i - 1], "Result mismatch");
			} catch (AssertionError e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			System.out.println(driver.findElement(By.xpath(Propobj.getProperty("ColumnFilterResultXpath1")+i+Propobj.getProperty("ColumnFilterResultXpath2"))).getText());
			driver.findElement(By.xpath(Propobj.getProperty("LastNameFilterXpath"))).clear();
			driver.navigate().refresh();
			driver.findElement(By.xpath(Propobj.getProperty("FilterToggleButton2"))).click();

			System.out.println("Test case passed without errors");

		}
	}

	@Test(priority = 3)
	public void fourthScenario() throws InterruptedException {
		System.out.println();
		System.out.println("Fourth scenario");
		driver.get(Propobj.getProperty("BaseURL"));
		wait = new WebDriverWait(driver, 20);
		driver.findElement(By.xpath(Propobj.getProperty("TableTabXpath"))).click();
		driver.findElement(By.xpath(Propobj.getProperty("TableSortXpath"))).click();
		Select dd = new Select(driver.findElement(By.xpath(Propobj.getProperty("EntriesDDXpath"))));
		dd.selectByValue("100");
		WebElement element = driver.findElement(By.xpath(Propobj.getProperty("AgeXpath")));
		element.click();
		wait.until(ExpectedConditions.attributeToBe(element, "aria-sort", "ascending"));
		//Thread.sleep(5000);
		String str = driver.findElement(By.xpath(Propobj.getProperty("AgeXpath"))).getAttribute("aria-sort");
		System.out.println(str);

		if (str.equalsIgnoreCase("ascending")) {
			System.out.println("Already in ascending order no need to sort again");
		} else {
			driver.findElement(By.xpath(Propobj.getProperty("AgeXpath"))).click();
		}

		String pos = driver.findElement(By.xpath(Propobj.getProperty("PositionXpath"))).getText();
		driver.findElement(By.xpath(Propobj.getProperty("SearchBoxXpath"))).sendKeys(pos);
		List<WebElement> resultList = driver.findElements(By.xpath(Propobj.getProperty("CountAfterSearchXpath")));
		System.out.println(resultList.size());
		System.out.println(driver.findElement(By.id(Propobj.getProperty("SearchResultID"))).getText());
		List<WebElement> tableResultList = driver.findElements(By.xpath(Propobj.getProperty("ResultsXpath")));
		for (WebElement webElement : tableResultList) {
			System.out.println(webElement.getText());
		}

	}
}
